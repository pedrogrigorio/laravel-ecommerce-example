FROM wyveo/nginx-php-fpm:php73

COPY . /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf

# Install system dependencies
RUN apt-get update && apt-get install -y \
    git \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    zip \
    unzip \
    openssl

RUN apt-get -y install sudo

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Npm
RUN  apt-get update -yq \
    && apt-get install curl gnupg -yq \
    && curl -sL https://deb.nodesource.com/setup_8.x | bash \
    && apt-get install nodejs -yq

RUN sudo apt-get install -yq php7.3-sqlite

#ENV NODE_VERSION=18.0.0
#RUN apt install -y curl
#RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash
#ENV NVM_DIR=/root/.nvm
#RUN . "$NVM_DIR/nvm.sh" && nvm install ${NODE_VERSION}
#RUN . "$NVM_DIR/nvm.sh" && nvm use v${NODE_VERSION}
#RUN . "$NVM_DIR/nvm.sh" && nvm alias default v${NODE_VERSION}
#ENV PATH="/root/.nvm/versions/node/v${NODE_VERSION}/bin/:${PATH}"


# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composerdocker

WORKDIR /usr/share/nginx/html

RUN ln -s public html
RUN apt update; \
    apt install vim -y

#RUN composer install
#RUN composer update
#RUN php artisan key:generate

RUN sudo apt-get install npm -y
RUN npm install
RUN npm run dev

RUN chmod +x run.sh
RUN sudo apt-get install -yq dos2unix
#COPY run.sh /run.sh
#ENTRYPOINT "/usr/share/nginx/html/run.sh"
#CMD bash -c "dos2unix run.sh"
#CMD bash -c "composer install && php artisan key:generate && php artisan ecommerce:install --force && php artisan serve --host 0.0.0.0"
EXPOSE 8000
