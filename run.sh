#!/bin/bash
composer install
php artisan key:generate
php artisan ecommerce:install --force
php artisan serve --host 0.0.0.0